package com.nespresso.recruitment.gossip;

import com.nespresso.recruitment.gossip.people.Person;

public class Gossip {

	Person person;
	String gossip;
	int spreadIndex=0;
	

	public Gossip(String gossip){
		this.gossip=gossip;
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	public String getGossip() {
		return gossip;
	}
	
	public String toString(){
		return gossip;
	}

	public int getSpreadIndex() {
		return spreadIndex;
	}

	public void setSpreadIndex(int spreadIndex) {
		this.spreadIndex = spreadIndex;
	}
}
