package com.nespresso.recruitment.gossip;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.nespresso.recruitment.gossip.people.Agent;
import com.nespresso.recruitment.gossip.people.Doctor;
import com.nespresso.recruitment.gossip.people.Mister;
import com.nespresso.recruitment.gossip.people.PeopleFactory;
import com.nespresso.recruitment.gossip.people.Person;

public class Gossips {

	Map<String,Person> people;
	List<Gossip> gossips;
	Person fromPerson=null;;
	Gossip gossip=null;
	int spreadIndex=0;
	
	public Gossips(String ...names){
		people= new HashMap<String,Person>();
		gossips= new ArrayList<Gossip>();
		for(String completeName: names){
			Person person=PeopleFactory.getPeople(completeName);
			people.put(person.getName(), person);
		}
	}
	
	public Gossips from(String person){
		fromPerson=people.get(person);
		return this;
	}
	
	public Gossips to(String person){
		Person nextPerson=people.get(person);
		if(fromPerson!=null){
			fromPerson.setNext(nextPerson);
			fromPerson=null;
		}
		else if(gossip!=null){
			gossips.add(this.gossip);
			this.gossip.setPerson(nextPerson);
			nextPerson.receiveGossip(gossip);
			gossip=null;
		}
		return this;
	}
	
	public Gossips say(String gossip){
		this.gossip=new Gossip(gossip);
		return this;
	}
	
	public String ask(String person){
		return people.get(person).ask();
		
	}
	
	public void spread(){
		spreadIndex++;
		Iterator<Gossip> iter= gossips.iterator();
		while(iter.hasNext())
		{
			Gossip goss=iter.next();
			
			Person person=goss.getPerson();
			Person nextPerson=person.getNext();
			
			if(nextPerson!=null){
				if(nextPerson instanceof Mister || nextPerson instanceof Doctor)
					spreadByMisterAndDoctor(person, nextPerson, goss);
				if(nextPerson instanceof Agent)
					spreadByAgen(person, (Agent)nextPerson, goss);
			}
			else
				iter.remove();
		}
	}
	
	
	private void spreadByMisterAndDoctor(Person person,Person nextPerson,Gossip goss){
		if(nextPerson.getGossip()==null)
		{
			person.getNext().receiveGossip(goss);
			goss.setPerson(person.getNext());
			person.receiveGossip(null);
			goss.setSpreadIndex(spreadIndex);
		}
		else if(nextPerson.getGossip().getSpreadIndex()!=spreadIndex)
		{
			person.getNext().receiveGossip(goss);
			goss.setPerson(person.getNext());
			person.receiveGossip(null);
			goss.setSpreadIndex(spreadIndex);
		}
	}
	
	
	private void spreadByAgen(Person person,Agent nextPerson,Gossip goss){
		if(nextPerson.getGossip()==null)
		{
			person.getNext().receiveGossip(goss);
			goss.setPerson(person.getNext());
			person.receiveGossip(null);
			goss.setSpreadIndex(spreadIndex);
		}
		else if(nextPerson.getGossip().getSpreadIndex()!=spreadIndex)
		{
			person.getNext().receiveGossip(goss);
			goss.setPerson(person.getNext());
			person.receiveGossip(null);
			goss.setSpreadIndex(spreadIndex);
		}
	}

}
