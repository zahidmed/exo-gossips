package com.nespresso.recruitment.gossip.people;

import com.nespresso.recruitment.gossip.Gossip;

public abstract class Person {

	String name;

	Person next;
	
	Gossip gossip;
	
	public Person(String name){
		this.name=name;
	}

	
	public boolean hasNex(){
		return next!=null;
	}
	
	public void receiveGossip(Gossip gossip) {
		// TODO Auto-generated method stub
		this.gossip=gossip;
	}

	
	public Gossip getGossip() {
		// TODO Auto-generated method stub
		return gossip;
	}
	
	
	public abstract String ask();
	
	public void setNext(Person next) {
		this.next = next;
	}
	public String getName() {
		return name;
	}

	public Person getNext() {
		return next;
	}
	
}
