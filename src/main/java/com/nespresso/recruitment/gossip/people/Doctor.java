package com.nespresso.recruitment.gossip.people;

import java.util.ArrayList;
import java.util.List;

import com.nespresso.recruitment.gossip.Gossip;

public class Doctor extends Person{

	List<Gossip> gossipHistory= new ArrayList<Gossip>();
	
	public Doctor(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}


	@Override
	public String ask() {
		// TODO Auto-generated method stub
		
		StringBuilder buffer= new StringBuilder("");
		int loop=0;
		for(Gossip goss: gossipHistory){
			if(loop!=0)
				buffer.append(", ");
			buffer.append(goss.getGossip());
			loop++;
		}
		return buffer.toString();
	}
	
	@Override
	public void receiveGossip(Gossip gossip) {
		// TODO Auto-generated method stub
		this.gossip=gossip;
		if(gossip!=null) 
			gossipHistory.add(gossip);
	}

}
