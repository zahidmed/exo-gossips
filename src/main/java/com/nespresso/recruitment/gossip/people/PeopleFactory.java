package com.nespresso.recruitment.gossip.people;

public class PeopleFactory {

	public static Person getPeople(String completeName){
		if(completeName.startsWith("Mr"))
			return new Mister(completeName.replaceFirst("Mr ", ""));
		if(completeName.startsWith("Dr"))
			return new Doctor(completeName.replaceFirst("Dr ", ""));
		else
			return  null;
	}
}
